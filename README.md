#Callbacks [![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=56f2f92b08226101000fd990&branch=master&build=latest)](https://dashboard.buddybuild.com/apps/56f2f92b08226101000fd990/build/latest)

Callbacks is a helper project written in Objective-C and a tiny bit of Swift that illustrates a few types of callback techniques.  This project contains a Person class that indicates completion of a particular task using the following approaches:

* Target-Action
* Delegates
* Completion Blocks
* Completion Blocks as Delegates 
* Notification Center
* Reactive Cocoa (RAC)

ViewController.m
```
- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self registerNotificationCenter];
    
    Person *person = [[Person alloc]init];
    person.firstName = @"Joe the plumber";
    person.delegate = self; // If anything interesting happens to person, this gets notified.
    
    NSLog(@"2) Synchronous: speakFirstName: %@.",[person speakFirstName]);
    
    NSString *firstName = [person speakWithCompletionHandler:^(double a, int b) {
        NSLog(@"3) Completion block: a=%f, b=%d.", a, b);
    }];
    
    NSLog(@"4) Synchronous: speakFirstName: %@.",firstName);
    
    [person speakNotifyWithNSNotificationCenter];
    
    self.myPerson = [[Person alloc]init];
    __weak typeof(self) weakSelf = self;
    self.myPerson.didVoteBlock = ^{
        NSLog(@"Person voted for: %@", weakSelf.myPerson.votedPresident);
    };
}
```

## Target-Action

Target-Action is the simplest callback mechanism to understand.  This is commonly associated code that is executed in response to user input, for example pressing a button.

### Delegates

Delegates are implemented by adding two protocols to the header file of a concrete class.  One protocol defines the delegate methods, the other protocol contains the protocol that defined the delegate methods.  Click [here](https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/CodingGuidelines/Articles/NamingMethods.html) for Apple's delegate naming convention.

##### Objective-C

Header file:
```
// Protocol which contains our Delegate protocol
@protocol PersonDelegate;

@protocol PersonProtocol <NSObject>
@property (nonatomic, weak) id<PersonDelegate> delegate; 
 ...
@end


// Delegate protocol
@protocol PersonDelegate <NSObject>
- (void)person:(id<PersonProtocol>)person didFinishSpeakingNameWithError:(NSError *)error;
- (void)personDidVote:(id<PersonProtocol>)sender;
@end


// Standard Objective-C interface for our class
@interface Person : NSObject <PersonProtocol>
   ...
@end
```

Implemenation file:
```
- (NSString *)speakFirstName {
    if ([self.delegate respondsToSelector: @selector (Person:didFinishSpeakingNameWithError:)]) {
        [self.delegate Person:self didFinishSpeakingNameWithError:nil];
    }
    return self.firstName;
}
```

##### Swift

Implementation:
```
@objc protocol CatDelegate {
    func cat(cat: Cat, didFinishEating snackySnack: Int)
}

class Cat: NSObject {
    var firstName = ""
    weak var delegate: CatDelegate?

    func eat() {
        delegate?.cat(self, didFinishEating: 3)
    }
}
```

Consuming the delegate:
```
    @interface ViewController () <CatDelegate>
    @end

    @implementation ViewController

    - (void)viewDidLoad {
        Cat *cat = [[Cat alloc] init];
        cat.delegate = self;
        [cat eat];
    }
    
    #pragma mark - CatDelegate

    - (void)cat:(Cat *)cat didFinishEating:(NSInteger)snackySnack {
        NSLog(@"Cat ate %ld snacks.", (long)snackySnack);
    }
    
```


## Completion Blocks

Typedefs can make blocks easier to read and understand.  

```
typedef void (^personSpokeBlock)(double, int); // This block returns a double and int.

- (NSString *)speakWithCompletionHandler:(personSpokeBlock)callbackBlock;
```

### Completion Blocks as Delegates 

ViewController.m
```
self.myPerson = [[Person alloc]init];
__weak typeof(self) weakSelf = self;
self.myPerson.didVoteBlock = ^{
    NSLog(@"Person voted for: %@", weakSelf.myPerson.votedPresident);
};
```
Person.h
```
typedef void (^voidBlock)();
@property (copy) voidBlock didVoteBlock;
```
Person.m
```
- (void)attemptToVote {
    self.votedPresident = @"Mickey Mouse";
    self.didVoteBlock();
}
```

## NSNotificationCenter

NSNotificationCenter is essentially a pub/sub mechanism that lives in your app.  For better or for worse, the scope of NSNotificationCenter is the entire app.  This can make debugging things tricky and for this reason, NSNotification is not considered to be a clean way of indicating an event or linking two distant objects together.

## Reactive Cocoa (RAC)

The worst option of them all. Do not click on this [link](https://github.com/ReactiveCocoa/ReactiveCocoa). 

## Console Output

```
2015-09-24 14:25:53.922 Callbacks[1491:1021572] 1) Delegate: didFinishSpeakingNameWithError: Joe the plumber.
2015-09-24 14:25:53.923 Callbacks[1491:1021572] 2) Synchronous: speakFirstName: Joe the plumber.
2015-09-24 14:25:53.923 Callbacks[1491:1021572] 3) Completion block: a=1.110000, b=2.
2015-09-24 14:25:53.923 Callbacks[1491:1021572] 4) Synchronous: speakFirstName: Joe the plumber.
2015-09-24 14:25:53.923 Callbacks[1491:1021572] 5) NSNotificationCenter: Joe the plumber.
2015-09-24 14:25:53.923 Callbacks[16011:4586354] Person is attempting to vote...
2015-09-24 14:25:53.924 Callbacks[16011:4586354] Person voted for: Mickey Mouse
```

<img src=https://raw.githubusercontent.com/mburolla/ReadMePix/master/HelperRepos/hr.png width=1024 height=100>
