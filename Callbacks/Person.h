//
//  Person.h
//  Callbacks
//
//  Created by Marty Burolla on 9/24/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

@import Foundation;

typedef void (^personSpokeBlock)(double, int);
typedef void (^voidBlock)();

////////////////////////////////////////////////////////////////////////////////////////////////////
// Protocol which contains our Delegate protocol
////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol PersonDelegate;

@protocol PersonProtocol <NSObject>
@property (nonatomic, weak) id<PersonDelegate> delegate; // weak to avoid a strong ref cycle.
@property (nonatomic, strong) NSString* firstName;
- (NSString *)speakFirstName;
@end


////////////////////////////////////////////////////////////////////////////////////////////////////
// Delegate protocol
////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol PersonDelegate <NSObject>
- (void)person:(id<PersonProtocol>)person didFinishSpeakingNameWithError:(NSError *)error;
- (void)personDidVote:(id<PersonProtocol>)sender;
@end


////////////////////////////////////////////////////////////////////////////////////////////////////
// Standard Objective-C interface to our class
////////////////////////////////////////////////////////////////////////////////////////////////////

@interface Person : NSObject <PersonProtocol>
@property (nonatomic, strong) NSString* lastName;
@property (copy) voidBlock didVoteBlock;
@property (nonatomic, strong) NSString* votedPresident;
@property (assign) int age;
- (NSString *)speakWithCompletionHandler:(personSpokeBlock)callbackBlock;
- (NSString *)speakNotifyWithNSNotificationCenter;
- (void)attemptToVote;
@end