//
//  CallbacksGlobals.h
//  Callbacks
//
//  Created by Marty Burolla on 9/24/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#ifndef CallbacksGlobals_h
#define CallbacksGlobals_h

#define NSNOTIFICATION_PERSON_SPOKE @"PersonSpoke"

#endif /* CallbacksGlobals_h */
