//
//  Person.m
//  Callbacks
//
//  Created by Marty Burolla on 9/24/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import "Person.h"
#import "CallbacksGlobals.h"


@interface Person()
@property (nonatomic, strong) NSString *SSN; // Private property in the class extension.
@end

@implementation Person

@synthesize firstName; // Properties in a protocol must be synthesized.
@synthesize delegate;


#pragma mark - Construction

-(instancetype) init {
    self = [super init];
    if (self!=nil) {
        _SSN = @"1111111";
        _lastName = @"";
        self.firstName = @"";
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Speak

- (NSString *)speakFirstName {
    if ([self.delegate respondsToSelector: @selector (person:didFinishSpeakingNameWithError:)]) {
        // Simulate delayed server side work.
        [self.delegate person:self didFinishSpeakingNameWithError:nil];
    }
    return self.firstName;
}

#pragma mark - Vote

- (void)attemptToVote {
    self.votedPresident = @"Mickey Mouse";
    self.didVoteBlock();
}

#pragma mark - Completion handlers

- (NSString *)speakWithCompletionHandler:(personSpokeBlock)callbackBlock {
    // Simulate delayed server side work.
    callbackBlock(1.11f, 2);
    return self.firstName;
}


#pragma mark - Notification Center

- (NSString *)speakNotifyWithNSNotificationCenter {
    [[NSNotificationCenter defaultCenter] postNotificationName:NSNOTIFICATION_PERSON_SPOKE object:self];
    return self.firstName;
}


@end