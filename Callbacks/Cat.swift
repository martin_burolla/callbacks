//
//  Cat.swift
//  Callbacks
//
//  Created by Martin Burolla on 3/23/16.
//  Copyright © 2016 Marty Burolla. All rights reserved.
//

import UIKit

@objc protocol CatDelegate {
    func cat(cat: Cat, didFinishEating snackySnack: Int)
}

class Cat: NSObject {
    
    // MARK: - Data members
    
    var firstName = ""
    weak var delegate: CatDelegate?

    // MARK: - Behavior
    
    func eat() {
        delegate?.cat(self, didFinishEating: 3) // Optional chaining avoids the need for a check here.
    }
}
