//
//  ViewController.m
//  Callbacks
//
//  Created by Marty Burolla on 9/24/15.
//  Copyright © 2015 Marty Burolla. All rights reserved.
//

#import "ViewController.h"
#import "Person.h"
#import "CallbacksGlobals.h"
#import "Callbacks-swift.h"

@interface ViewController () <PersonDelegate, CatDelegate>
@property (nonatomic, strong) Person *myPerson;
@end

@implementation ViewController


#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerNotificationCenter];
    
    //
    // Objective C
    //
    
    Person *person = [[Person alloc] init];
    person.firstName = @"Joe the plumber";
    person.delegate = self; // If anything interesting happens to person, this gets notified.
    
    NSLog(@"2) Synchronous: speakFirstName: %@.",[person speakFirstName]);
    
    NSString *firstName = [person speakWithCompletionHandler:^(double a, int b) {
        NSLog(@"3) Completion block: a=%f, b=%d.", a, b);
    }];
    
    NSLog(@"4) Synchronous: speakFirstName: %@.",firstName);
    
    [person speakNotifyWithNSNotificationCenter];
    
    self.myPerson = [[Person alloc] init];
    __weak typeof(self) weakSelf = self;
    self.myPerson.didVoteBlock = ^{
        NSLog(@"Person voted for: %@", weakSelf.myPerson.votedPresident);
    };
    
    //
    // Swift
    //
    
    Cat *cat = [[Cat alloc] init];
    cat.delegate = self;
    [cat eat];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - PersonDelegate

- (void)person:(id<PersonProtocol>)person didFinishSpeakingNameWithError:(NSError *)error {
    NSLog(@"1) Delegate: didFinishSpeakingNameWithError: %@.",((Person *)person).firstName);
}

- (void)personDidVote:(id<PersonProtocol>)sender {
    NSLog(@"Person voted");
}


#pragma mark - CatDelegate

- (void)cat:(Cat *)cat didFinishEating:(NSInteger)snackySnack {
    NSLog(@"Cat ate %ld snacks.", (long)snackySnack);
}


#pragma mark - NotificationCenter

- (void)registerNotificationCenter {    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    [defaultCenter addObserver: self
                      selector: @selector(aPersonSpoke:)
                          name: NSNOTIFICATION_PERSON_SPOKE
                        object: nil];
}

- (void)aPersonSpoke: (NSNotification *)aNotification {
    Person *person = aNotification.object;
    NSLog(@"5) NSNotificationCenter: %@.",person.firstName);
}


#pragma mark - Target Action

- (IBAction)ButtonTouchUpInside:(id)sender {
    NSLog(@"Person is attempting to vote...");
    [self.myPerson attemptToVote];
}

@end